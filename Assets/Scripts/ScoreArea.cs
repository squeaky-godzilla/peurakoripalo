﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreArea : MonoBehaviour
{
    public ParticleSystem rearParticleSystem;

    public GameObject upperScoreArea;

    public Text trickAnnouncePopup;

    public ParticleSystem hitExplosion;
    public AudioSource rearAudioSystem;
    public AudioSource frontAudioSystem;
    public ParticleSystem frontParticleSystem1;
    public ParticleSystem frontParticleSystem2;
    public Player player;

    public GameObject ball;
    public GameObject hoop;
    // public float score = GameController.finalScore;

    // public bool ballPresent = false;

    IEnumerator AnnouncerEffectSequence (string Announcement, 
        Text trickAnnouncePopup,
        float rotationCoef,
        float scale,
        float effectDistanceScale = 1
        ) 
        {
        
        float annoucementRotation = 2f*rotationCoef;
        float flickerTimeout = 0.1f;
        float initialTimeout = 0.5f;
        // float effectDistanceScale = 0.5f;
        // trickAnnouncePopup.transform.localScale = trickAnnouncePopup.transform.localScale*scale;
        trickAnnouncePopup.transform.localScale = new Vector3(scale,scale,scale);


        for (int i = 0; i < 6; i++)
        {
        trickAnnouncePopup.enabled = true;
        trickAnnouncePopup.text = Announcement;
        trickAnnouncePopup.GetComponent<Outline> ().effectDistance = new Vector2 (30,30)*effectDistanceScale;
        trickAnnouncePopup.GetComponent<AudioSource> ().Play();
        trickAnnouncePopup.GetComponent<Outline> ().effectDistance = new Vector2 (30,30*effectDistanceScale);
        // trickAnnouncePopup.text = Announcement;
        trickAnnouncePopup.GetComponent<Outline> ().effectDistance = new Vector2 (10,10)*effectDistanceScale;
        yield return new WaitForSeconds(flickerTimeout);
        trickAnnouncePopup.transform.eulerAngles = (new Vector3 (0f,0f,-annoucementRotation));
        trickAnnouncePopup.GetComponent<Outline> ().effectDistance = new Vector2 (30,30)*effectDistanceScale;
        yield return new WaitForSeconds(flickerTimeout);
        trickAnnouncePopup.transform.eulerAngles = (new Vector3 (0f,0f,-annoucementRotation));
        trickAnnouncePopup.transform.eulerAngles = (new Vector3 (0f,0f,0));
        trickAnnouncePopup.transform.eulerAngles = (new Vector3 (0f,0f,annoucementRotation));

        }
        trickAnnouncePopup.enabled = false;

    }

    public string[] goodCommentsList = {
        "Decent!",
        "Not Bad!",
        "Good!"
    };

    public string[] greatCommentsList = {
        "Great shot!!",
        "Awesome!!",
        "Swish!!",
        "Money!!",
        "Like a pro!!"
    };

    public string[] ultraCommentsList = {
        "Holy shot!!!",
        "Oh deer!!!",
        "Sniper skills!!!",
        "Savage!!!",
        "Brutal!!!",
        "Sick shot!!!"
    };

    public string[] spaceCommentsList = {
        "Huge step for mankind!!!",
        "Apollo 13!!!",
        "Such space program",
        "Liftoff!!!",
        "To boldly go!!!",
        "Spaceballs"
    };

    public string GetRandomComment(string[] commentsList) {
        // var list = new List<string>{ "one","two","three","four"};
        int index = Random.Range(0, commentsList.Length);
        return commentsList[index];
    }

    private float CountScore(float Distance) {
        float throwScore = Distance;
        return throwScore;
    }

    void ReactToThrowScore(float throwScore) {
        if (throwScore <= 10) {
            string comment = GetRandomComment(goodCommentsList);
            StartCoroutine(AnnouncerEffectSequence(comment, trickAnnouncePopup, 0.1f, 1f));
        }
        if (throwScore > 10) {
            Debug.Log("reacting to score: " + throwScore);
            rearParticleSystem.Play();
            rearAudioSystem.Play();
            hitExplosion.Play();
            string comment = GetRandomComment(greatCommentsList);
            StartCoroutine(AnnouncerEffectSequence(comment, trickAnnouncePopup, 0.1f, 1f));
        }
        if (throwScore > 20) {
            frontParticleSystem1.Play();
            frontParticleSystem2.Play();
            frontAudioSystem.Play();
            string comment = GetRandomComment(ultraCommentsList);
            StartCoroutine(AnnouncerEffectSequence(comment, trickAnnouncePopup, 0.1f, 1f));
        }
        if (throwScore > 100) {
            string comment = GetRandomComment(spaceCommentsList);
            StartCoroutine(AnnouncerEffectSequence(comment, trickAnnouncePopup, 0.1f, 1f, 0.3f));
        }
    }

    // void Update() {
    //     Debug.Log("scoringUnlocked = " + upperScoreArea.GetComponent<UpperScoreArea> ().scoringUnlocked);
    // }

    void OnTriggerEnter(Collider other) {
        if ((other.GetComponent<Ball> () != null) && (player.holdingBall != true) && (upperScoreArea.GetComponent<UpperScoreArea> ().scoringUnlocked == true)) {
        float throwScore = CountScore(Vector3.Distance(player.shootingLocation, transform.position));
        GameController.finalScore += throwScore;
        Debug.Log ("Score: " + throwScore);
        ReactToThrowScore(throwScore);
        GetComponent<AudioSource> ().Play();
        // ballPresent = false;
        }
        // upperScoreArea.GetComponent<UpperScoreArea> ().scoringUnlocked = false;
    }

}
