﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource boardSound;

    void OnCollisionEnter(Collision other) {
        boardSound.Play();
    }

}
