﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationaryPlayer : MonoBehaviour
{
    // Start is called before the first frame update

    public Ball ball;
    public float ballThrowingForce;
    public float ballThrowingForceChargeSpeed;
    public float ballElevation;
    public float ballDistance;
    public bool holdingBall;
    public bool isCharging;
    public Camera playerCamera;
    public AudioSource chargingSound;
    public AudioSource shotSound;
    public Vector3 shootingLocation;


    void Start()
    {
        ball.GetComponent<Rigidbody> ().useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (holdingBall) {
            ball.transform.position = playerCamera.transform.position + playerCamera.transform.forward * ballDistance + playerCamera.transform.up * ballElevation;
            if (Input.GetMouseButtonDown(0)) {

                chargingSound.time = 2;
                chargingSound.Play();
                isCharging = true;
            }
            if (isCharging) {

                ballThrowingForce += Time.deltaTime * ballThrowingForceChargeSpeed;
                // ballThrowingForce += Time.deltaTime;
                // Debug.Log("charging ...");
            }    
            if (Input.GetMouseButtonUp(0)) {
                
                chargingSound.Stop();
                shootingLocation = playerCamera.transform.position;
                holdingBall = false;
                isCharging = false;
                ball.ActivateTrail ();
                shotSound.Play();
                ball.GetComponent<Rigidbody> ().useGravity = true;
                ball.GetComponent<Rigidbody> ().freezeRotation = false;
                ball.GetComponent<Rigidbody> ().AddForce ((playerCamera.transform.up*0.3f + playerCamera.transform.forward) * ballThrowingForce);
                ball.GetComponent<Rigidbody> ().AddTorque (-400f, 0f, 0f);
                // Debug.Log("Force: " + ballThrowingForce);
                ballThrowingForce = 0f;
                }
            }
    }
}
