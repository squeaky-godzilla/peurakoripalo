﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // public GameObject ball;
    public Ball ball;
    public GameObject playerCamera;
    public float ballDistance = 2f;
    public float ballElevation = 0.5f;
    public float ballThrowingForce = 200f;
    public float ballThrowingForceLimit = 1000f;
    public float ballThrowingForceChargeSpeed = 1000f;
    public bool holdingBall = true;

    public Vector3 shootingLocation;
    public AudioSource shotSound;

    public AudioSource chargingSound;

    // void shotSound = Player.shotSound.GetComponent<AudioSource> ()

    private bool isCharging = false;
    // Start is called before the first frame update

    void Start()
    {
        ball.GetComponent<Rigidbody> ().useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (holdingBall) {
            ball.transform.position = playerCamera.transform.position + playerCamera.transform.forward * ballDistance + playerCamera.transform.up * ballElevation;
            if (Input.GetMouseButtonDown(0)) {

                chargingSound.time = 2;
                chargingSound.Play();
                isCharging = true;
            }
            if (isCharging) {

                ballThrowingForce += Time.deltaTime * ballThrowingForceChargeSpeed;
                // ballThrowingForce += Time.deltaTime;
                // Debug.Log("charging ...");
            }    
            if (Input.GetMouseButtonUp(0)) {
                
                chargingSound.Stop();
                shootingLocation = playerCamera.transform.position;
                holdingBall = false;
                isCharging = false;
                ball.ActivateTrail ();
                shotSound.Play();
                ball.GetComponent<Rigidbody> ().useGravity = true;
                ball.GetComponent<Rigidbody> ().freezeRotation = false;
                ball.GetComponent<Rigidbody> ().AddForce ((playerCamera.transform.up*0.3f + playerCamera.transform.forward) * ballThrowingForce);
                ball.GetComponent<Rigidbody> ().AddTorque (-400f, 0f, 0f);
                // Debug.Log("Force: " + ballThrowingForce);
                ballThrowingForce = 0f;
                }
            }
    }
}
