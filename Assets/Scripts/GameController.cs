﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameController : MonoBehaviour
{
    public Ball ball;
    // public GameObject playerCamera;
    public Player player;

    public ScoreArea scoreArea;

    public DefenseDeer defenseDeer;
    // public Scenario.DefenceDeer defenceDeer;
    public ParticleSystem teleportEffect;
    public AudioSource teleportSFX;

    AudioSource soundtrack;
    public float resetTimer = 5f;
    public float timeElapsed;
    public float gameTimeLimit = 158f;
    public float totalTimeElapsed;

    public static float finalScore;
    public static int currentLevelIndex = 0;

    public static string[] availableLevels={
        "Game2",
        "Game3",
        "Ending"
    };

    public void deerTeleportEffect () {
        
        // teleportEffect.Play();
        teleportSFX.Play();
    }

    public GameObject deerWords;
    private string[] deerQuotes = {
        "hey, you!",
        "i'm the defense!",
        "you got nothing!",
        "lorem ipsum bro!",
        "blocking!",
        "watch the antlers dude!",
        "i'm the latest in deer defense technology!"
    };

    public string GetRandomComment(string[] commentsList) {
        
        int index = Random.Range(0, commentsList.Length);
        return commentsList[index];
    }

    void changeDefense () {
        Vector3 playerPos = player.transform.position;
        Vector3 playerDirection = player.transform.forward;
        Quaternion playerRotation = player.transform.rotation;
        float spawnDistance = 6;
        float spawnHeight = -1.35f;

        // Vector3 spawnPos = playerPos + playerDirection*spawnDistance + player.transform.up*spawnHeight + player.transform.right*(-1f);
        Vector3 spawnPos = playerPos + playerDirection*spawnDistance + player.transform.right*(-1f);
        // defenseDeer.transform.position = spawnPos;
        spawnPos.Set(spawnPos.x,spawnHeight,spawnPos.z); //set the deer on ground
        defenseDeer.transform.position = spawnPos;
        defenseDeer.transform.rotation = playerRotation;
        string currentDeerQuote = GetRandomComment(deerQuotes);
        deerWords.GetComponent<TextMeshPro> ().text = currentDeerQuote;
        // Debug.Log(currentDeerQuote);
        


        // deerTeleportEffect();
    }

    public void resetBall () {
        // ball.transform.position = playerCamera.transform.position + playerCamera.transform.forward * player.ballDistance;
        // ball.transform.position = player.playerCamera.transform.position + player.playerCamera.transform.forward * player.ballDistance;
        player.holdingBall = true;
        ball.DeactivateTrail();
        ball.GetComponent<Rigidbody> ().useGravity = false;
        ball.GetComponent<Rigidbody> ().freezeRotation = true;
        ball.transform.rotation = Quaternion.identity;
        ball.GetComponent<Rigidbody> ().velocity = new Vector3(0f,0f,0f);
        ball.GetComponent<Rigidbody> ().angularVelocity = new Vector3(0f,0f,0f);
        changeDefense();

        // rigidbody.velocity = new Vector3(0f,0f,0f); 
        // rigidbody.angularVelocity = new Vector3(0f,0f,0f);
        // transform.Rotation = Quaternion.Euler(new Vector3(0f,0f,0f);

    }

    // Start is called before the first frame update
    void Start()
    {
        // soundtrack.Play(0);

    }

    // Update is called once per frame
    void Update()
    {
        if (player.holdingBall == false) {
            timeElapsed += Time.deltaTime;
            if ((timeElapsed >= resetTimer) || (Input.GetMouseButtonDown(1))) {
                // SceneManager.LoadScene ("Game");
                resetBall();
                timeElapsed = 0f;
            }
        }
        totalTimeElapsed += Time.deltaTime;
        if (totalTimeElapsed >= gameTimeLimit) {
            // player.enabled = false;
            // finalScore = scoreArea.score;
            currentLevelIndex += 1;
            SceneManager.LoadScene (availableLevels[currentLevelIndex]);
            
            // SceneManager.LoadScene ("Ending");
        }

        // if (totalTimeElapsed % 13 == 0) {
        //     deerWords.GetComponent<TextMeshPro> ().text = GetRandomComment(deerQuotes);
        // }
        

    }
}
