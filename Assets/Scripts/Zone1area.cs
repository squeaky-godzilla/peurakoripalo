﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zone1area : MonoBehaviour

{
    public bool inZone = false;
    public float zoneMultiplier = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
     void OnTriggerEnter(Collider other) {
        if (other.GetComponent<Player> () != null) {
        Debug.Log("entered zone 1");
        inZone = true;
        }
     }
     
     void OnTriggerExit(Collider other) {
        if (other.GetComponent<Player> () != null) {
        Debug.Log("exited zone 1");
        inZone = false;
        }
     }
}
