﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FinalScoreDisplay : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Cursor.visible = true;
        GetComponent<TextMeshPro> ().text = "Score: " + GameController.finalScore;
    }

    
    
    // Update is called once per frame
    void Update()
    {
        // if (Input.GetMouseButtonDown(0)) {

        //     UnityEngine.Application.Quit();    
        // }
    }
}
