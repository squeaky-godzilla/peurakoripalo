﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Satellite : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public Vector3 rotationIncrement;
    void Update()
    {   


        // float speed = 10;
        // float count = 0;
        // float circumference = 500;
        

        // count += Time.deltaTime * speed;
        // var x = Mathf.Cos(count) * circumference;
        // var y = Mathf.Sin(count) * circumference;
        // Vector3 targetPosition = new Vector3(x, 100, y);
        // transform.position = targetPosition + new Vector3(0,100,0);

        // Satellite Rotation here
        transform.Rotate(rotationIncrement * Time.deltaTime);

    }
}
