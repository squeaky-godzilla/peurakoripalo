﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpperScoreArea : MonoBehaviour


{
    public GameObject scoreArea;
    public bool scoringUnlocked = false;
    // Start is called before the first frame update
    void OnTriggerEnter(Collider other) {
        if ((other.GetComponent<Ball> () != null)) {
            scoringUnlocked = true;
    }
    }

    void OnTriggerExit(Collider other) {
        if (other.GetComponent<Ball> () != null) {
            scoringUnlocked = false;
    }
    }
}
